from django.http import JsonResponse, HttpResponse
from django.shortcuts import render

from IrriDesk.daos import FarmDao, FarmModel, CropPropertiesDao, SensorDao
from IrriDesk.daos.irrigation_sector_dao import IrrigationSectorDao
from IrriDesk.daos.soil_properties_dao import SoilPropertiesDao
from IrriDesk.models.irrigation_sector_model import IrrigationSectorModel


def main(request):
    return render(request, 'irrigation/home.html')


def show_weather_data(request):
    # return render(request, 'irrigation/html-weather.html')
    farms = FarmDao.get_all(False)
    return render(request, 'irrigation/html-new-farm.html', {'farms': farms, })


def show_soil_definition(request):
    soils = SoilPropertiesDao.get_all_type(False)
    return render(request, 'irrigation/html-soil.html', {'soils': soils})


def show_crop_definition(request):
    crops = CropPropertiesDao.get_all_groups_type()
    return render(request, 'irrigation/html-crop.html', {'crops': crops})


def show_irrigation_setup(request):
    return render(request, 'irrigation/html-irrigation.html')


def show_result(request):
    farms = FarmDao.get_all(False)

    return render(request, 'irrigation/html-result.html', {'farms': farms, })


def save_farm(request):
    label = request.POST.get('txt-farm-name')

    farmmodel = FarmDao.save(FarmModel(None, label, None))
    # farm_model = farm_dao.save()

    # return redirect('render_farm')
    return HttpResponse('')


def save_sector(request):
    label = request.POST.get('txt-sector-name')
    farm_id = request.POST.get('farm-id')

    farm = FarmDao.get(farm_id)

    if farm is not None:
        irr_model = IrrigationSectorModel()
        irr_model.label = label
        irr_model.farm = farm

        irr = IrrigationSectorDao.save(irr_model)
        # farm_model = farm_dao.save()

        print(irr)

    # return redirect('render_farm')
    return HttpResponse('')


def get_sectors(request, farm_id):
    s = IrrigationSectorDao.get_all_by_farm(farm_id, True)

    return JsonResponse({'list': s}, safe=False)


def get_farms(request):
    s = FarmDao.get_all(True)

    return JsonResponse({'list': s}, safe=False)


def get_features(request, sector_id):
    s = IrrigationSectorDao.get(sector_id, True)

    return JsonResponse(s, safe=False)


def get_html_add_farm(request):
    return render(request, 'irrigation/create-farm.html')


def get_html_add_sector(request, farm_id):
    return render(request, 'irrigation/create-sector.html', {'farm': farm_id})


def save_features(request):
    features = request.POST.get('features')
    farm_id = request.POST.get('farm_id')
    sector_id = request.POST.get('sector_id')

    farm = FarmDao.get(farm_id)
    sector = IrrigationSectorDao.get(sector_id, False)

    if farm is not None and sector is not None:
        sector.gis_features = features

        irr = IrrigationSectorDao.save_features(sector)
        # farm_model = farm_dao.save()

        print(irr)

    # return redirect('render_farm')
    return HttpResponse('')


def get_crops_from_group(request, crop_group):
    crops = CropPropertiesDao.get_all_crops_by_group(crop_group)
    return JsonResponse(crops, safe=False)


def get_crops_options_from_group(request, crop_group, crop):
    crops = CropPropertiesDao.get_all_crops_option_by_group(crop_group, crop)
    return JsonResponse(crops, safe=False)


def get_data_result(request, sector_id, date_i, date_e):
    print(sector_id, date_i, date_e)
    # data = SensorDao.get_data_by_sector_and_date(sector_id, date_i)
    data = SensorDao.get_data_by_sector_in_interval(sector_id, date_i, date_e)

    d = dict({'data': data})
    return JsonResponse(d, safe=False)


def get_series_result(request, farm_id, sector_id, date):
    # import json
    sensors = SensorDao.get_by_irrigation_sector(sector_id, True)
    data = list()
    for s in sensors:
        id = s['id']
        series = SensorDao.get_series_in_date(id, date, True)
        data.append(series)

    # series = SensorDao.get_series_in_date(1, date, True)
    # string_series = series[0]['data']
    # series[0]['data'] = list()
    # dj = json.loads(string_series)

    # dj = dj.sort(key=extract_time)
    # print(dj)

    # def sort_by_date(js):
    #    from datetime import datetime
    #
    #    return datetime.strptime(js['date'], "%d/%m/%Y %H:%M")

    # lis = sorted(dj, key=sort_by_date)
    # series[0]['data'] = json.dumps(lis)
    # data.append(series)

    # sensors = SensorDao.get_by_irrigation_sector(sector_id, True)
    d = dict({'sensor': sensors, 'series': data})

    return JsonResponse(d, safe=False)
