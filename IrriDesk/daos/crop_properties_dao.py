import datetime

from DBAPI.api.serializers import CropTypeSerializer
from DBAPI.models.crop_properties import CropProperties
from DBAPI.models.crop_type import CropType
from IrriDesk.models.crop_properties_model import CropPropertiesModel
from IrriDesk.models.crop_type_model import CropTypeModel


class CropPropertiesDao:
    id = ""

    crop_type = ""
    crop = ""
    crop_options = ""

    distance_trees = 0.0
    distance_lines = 0.0
    plantation_year = 0

    tree_width = 0.0
    tree_depth = 0.0
    tree_height = 0.0

    start_vegetative_period = datetime
    flowering_date = datetime
    start_harvest = datetime
    end_harvest = datetime

    # TODO: ask to Jaume about this dictionary
    # additional_properties = list()

    @staticmethod
    def new_instance(crop, is_type):
        c = CropPropertiesModel()
        c.id = crop.id

        c.crop_type = crop.crop_type
        c.crop = crop.crop
        c.crop_options = crop.crop_options

        if is_type is not True:
            c.distance_trees = crop.distance_trees
            c.distance_lines = crop.distance_lines
            c.plantation_year = crop.plantation_year

            c.tree_width = crop.tree_width
            c.tree_depth = crop.tree_depth
            c.tree_height = crop.tree_height

            c.start_vegetative_period = crop.start_vegetative_period
            c.flowering_date = crop.flowering_date
            c.start_harvest = crop.start_harvest
            c.end_harvest = crop.end_harvest

        return c

    @staticmethod
    def save(self):
        """ Create a new instance od farm in the database and return the object created """
        # f = CropProperties.create(crop_type=self.crop_type,
        #                           crop=self.crop,
        #                           crop_options=self.crop_options,
        #                           distance_trees=self.distance_trees,
        #                           distance_lines=self.distance_lines,
        #                           plantation_year=self.plantation_year,
        #                           tree_width=self.tree_width,
        #                           tree_height=self.tree_height,
        #                           tree_depth=self.tree_depth,
        #                           start_vegetative_period=self.start_vegetative_period,
        #                           start_harvest=self.start_harvest,
        #                           flowering_date=self.flowering_date,
        #                           end_harvest=self.end_harvest)
        # return self.new_instance(f)
        return None

    @staticmethod
    def save_crop_type(self):

        CropType.create(crop_type=self.crop_type, crop=self.crop, crop_options=self.crop_options)

    @staticmethod
    def update(self):
        # c = CropProperties.objects.get(id=self.id)
        # c.id = self.id
        #
        # c.crop_type = self.crop_type
        # c.crop = self.crop
        # c.crop_options = self.crop_options
        #
        # c.distance_trees = self.distance_trees
        # c.distance_lines = self.distance_lines
        # c.plantation_year = self.plantation_year
        #
        # c.tree_width = self.tree_width
        # c.tree_depth = self.tree_depth
        # c.tree_height = self.tree_height
        #
        # c.start_vegetative_period = self.start_vegetative_period
        # c.flowering_date = self.flowering_date
        # c.start_harvest = self.start_harvest
        # c.end_harvest = self.end_harvest
        # c.save()
        #
        # return self.new_instance(c)
        return None

    @staticmethod
    def delete(self):
        """ returns the number of objects deleted and a dictionary with the number of deletions per object type. """
        f = CropProperties.objects.get(id=self.id)
        return f.delete()

    @staticmethod
    def get_type(crop_type_id, serialize):
        obj = CropType.objects.get(crop_type_id)

        if serialize is False:
            return CropPropertiesDao.new_instance(obj, True)
        else:
            seria = CropTypeSerializer(obj, many=True)
            return seria.data

    @staticmethod
    def get_all_type(serialize):
        objects = CropType.objects.all()

        if serialize is False:

            crops = list()
            for f in objects:
                crops.append(CropPropertiesDao.new_instance(f, True))

            return crops

        else:
            seria = CropTypeSerializer(objects, many=True)
            return seria.data

    @staticmethod
    def get_all_groups_type():
        obj = CropType.objects.all()
        # create a dictionary to distinct the crop_type
        crops = dict([(c.crop_type, c.id) for c in obj])
        # create and return a list with the crop_types
        # return list([c for c in crops])
        return crops

    @staticmethod
    def get_all_crops_by_group(crop_group):
        obj = CropType.objects.filter(crop_type=crop_group)
        crops = dict([(c.crop, c.id) for c in obj])

        return crops

    @staticmethod
    def get_all_crops_option_by_group(crop_group, crop):
        obj = CropType.objects.filter(crop_type=crop_group, crop=crop)
        crops = dict([(c.crop_options, c.id) for c in obj])

        return crops
