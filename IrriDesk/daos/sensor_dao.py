from DBAPI.api.serializers import SensorDataSerializer, SensorSeriesSerializer, SensorSerializer
from DBAPI.models.sensor import Sensor
from DBAPI.models.sensor_data import SensorData
from DBAPI.models.sensor_series import SensorSeries
from IrriDesk.models.sensor_data_model import SensorDataModel
from IrriDesk.models.sensor_model import SensorModel
from IrriDesk.models.sensor_series_model import SensorSeriesModel

from django.db.models import Q
from datetime import datetime

class SensorDao:

    @staticmethod
    def new_instance(sensor):
        if sensor is None:
            return SensorModel(None, None)
        # return SensorModel(sensor.id, sensor.label, sensor.init_at, sensor.end_at, sensor.data)
        sensor = SensorModel(sensor.id, sensor.label)
        sensor.series = SensorDao.get_series(sensor.id)

        return sensor

    @staticmethod
    def new_instance_data(sensor):
        if sensor is None:
            return SensorDataModel(None, None, None, None, None, None, None, None, None)

        return SensorDataModel(sensor.id, sensor.acumm_irr, sensor.irr_day, sensor.eto, sensor.irr_ok, sensor.plan_ok,
                               sensor.sensor_ok, sensor.all_ok, sensor.date)

    @staticmethod
    def new_instance_series(sensor):
        if sensor is None:
            return SensorSeriesModel(None, None, None, None)

        return SensorSeriesModel(sensor.id, sensor.init_at, sensor.end_at, sensor.data)

    @staticmethod
    def save(sensor_model, sector):
        """ Create a new instance od farm in the database and return the object created """

        # s = Sensor.create(sensor_model.label, sector, sensor_model.init_at, sensor_model.end_at, sensor_model.data)
        s = Sensor.create(sensor_model.label, sector)
        s.save()

        return SensorDao.new_instance(s)

    @staticmethod
    def set_series(sensor, sensor_series):

        # convert to model of database
        sensor = Sensor.objects.get(id=sensor.id)

        s = SensorSeries.create(sensor.id, sensor_series.init_at, sensor_series.end_at, sensor_series.data)
        s.save()

        return SensorDao.new_instance(sensor)

    @staticmethod
    def set_data(irri_sector, sensor_data):

        s = SensorData.create(irri_sector.id, sensor_data.acumm_irr, sensor_data.irr_day, sensor_data.eto,
                              sensor_data.irr_ok, sensor_data.plan_ok, sensor_data.sensor_ok, sensor_data.all_ok,
                              sensor_data.date)
        s.save()

        # return SensorDao.new_instance(s)

    @staticmethod
    def delete(sector_model):
        """ returns the number of objects deleted and a dictionary with the number of deletions per object type. """
        f = Sensor.objects.get(id=sector_model.id)
        return f.delete()

    @staticmethod
    def get_by_irrigation_sector(sector_id, serializable):

        try:
            sensors = Sensor.objects.filter(irrigation_sector_id=sector_id)
        except Sensor.DoesNotExist:
            return None

        if serializable is True:
            seria = SensorSerializer(sensors, many=True)
            return seria.data
        else:
            all_s = list()
            for f in sensors:
                all_s.append(SensorDao.new_instance(f))

            return all_s

    @staticmethod
    def get_by_name(sensor_name, sector_id):

        try:
            sensor = Sensor.objects.get(irrigation_sector__id=sector_id, label=sensor_name)
        except Sensor.DoesNotExist:
            return None

        return SensorDao.new_instance(sensor)

    @staticmethod
    def get_data_by_sector(s_id, serializable):

        objects = SensorData.objects.filter(irrigation_sector__id=s_id)

        if serializable is True:
            seria = SensorDataSerializer(objects, many=True)
            return seria.data
        else:
            all_d = list()
            for f in objects:
                all_d.append(SensorDao.new_instance_data(f))

            return all_d

    @staticmethod
    def get_data_by_sector_and_date(s_id, date):
        # always return serializable
        dt = datetime.strptime(date, "%Y-%m-%d").date()
        objects = SensorData.objects.filter(irrigation_sector__id=s_id, date=dt)

        seria = SensorDataSerializer(objects, many=True)
        return seria.data

    @staticmethod
    def get_data_by_sector_in_interval(s_id, ini, fin):
        # always return serializable
        ini = datetime.strptime(ini, "%Y-%m-%d").date()
        fin = datetime.strptime(fin, "%Y-%m-%d").date()
        objects = SensorData.objects.filter(Q(date__lte=fin) & Q(date__gte=ini), irrigation_sector__id=s_id)\
            .order_by('date')

        seria = SensorDataSerializer(objects, many=True)
        return seria.data

    @staticmethod
    def get_series(sensor_id):

        objects = SensorSeries.objects.filter(sensor__id=sensor_id)
        sensor_d = list()
        for f in objects:
            sensor_d.append(SensorDao.new_instance_series(f))

        return sensor_d

    @staticmethod
    def get_series_in_date(s_id, date, serializable):

        dt = datetime.strptime(date, "%Y-%m-%d").date()
        objects = SensorSeries.objects.filter(Q(init_at__lte=dt) & Q(end_at__gte=dt), sensor__id=s_id)

        if serializable is True:
            s = SensorSeriesSerializer(objects, many=True)
            return s.data
        else:
            sensor_d = list()
            for f in objects:
                sensor_d.append(SensorDao.new_instance_series(f))

            return sensor_d
