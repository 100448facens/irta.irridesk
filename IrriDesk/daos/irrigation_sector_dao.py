import json
from bson import json_util
from DBAPI.api.serializers import IrrigationSectorSerializer
from DBAPI.models import IrrigationSector
from IrriDesk.daos import FarmDao, FarmModel, SensorDao, SensorModel, SensorSeriesModel
from IrriDesk.models.irrigation_sector_model import IrrigationSectorModel
from datetime import datetime


class IrrigationSectorDao:

    @staticmethod
    def new_instance(i):

        irr = IrrigationSectorModel()
        if i is not None:
            irr.id = i.id
            irr.label = i.label
            irr.farm = FarmDao.new_instance(i.farm)
            irr.crop = None
            irr.soil = None
            irr.irrigation_setup = None
            irr.gis_features = i.gis_features
            irr.sensors = SensorDao.get_by_irrigation_sector(i.id, False)
            irr.sensors_data = SensorDao.get_data_by_sector(i.id, False)

        return irr

    @staticmethod
    def save(irrigation_model):
        irrs = IrrigationSector.create(label=irrigation_model.label, farm=irrigation_model.farm)
        irrs.save()
        return IrrigationSectorDao.new_instance(irrs)

    @staticmethod
    def save_features(irrigation_model):

        c = IrrigationSector.objects.get(id=irrigation_model.id)
        c.gis_features = irrigation_model.gis_features
        c.save()

        # c.gis_features = irrigation_model.gis_features

        return IrrigationSectorDao.new_instance(c)

    @staticmethod
    def set_sensors(label, irrigation_model):

        c = IrrigationSector.objects.get(id=irrigation_model.id)

        sensors = irrigation_model.sensors
        init_at = sensors[0]['date']
        dt_init = datetime.strptime(init_at, "%d/%m/%Y %H:%M").date()
        end_at = sensors[len(sensors) - 1]['date']
        dt_end = datetime.strptime(end_at, "%d/%m/%Y %H:%M").date()
        data = json.dumps(sensors, ensure_ascii=False, default=json_util.default)

        sensor = SensorDao.get_by_name(label, c.id)

        if sensor is None:
            sensor = SensorDao.save(SensorModel(None, label), c)

        SensorDao.set_series(sensor, SensorSeriesModel(None, dt_init, dt_end, data))

        # return IrrigationSectorDao.new_instance(c)

    @staticmethod
    def get(sector_id, serializable):

        if serializable is True:
            sectors = IrrigationSector.objects.filter(id=sector_id)
            seria = IrrigationSectorSerializer(sectors, many=True)
            return seria.data
        else:
            try:
                sectors = IrrigationSector.objects.get(id=sector_id)
            except IrrigationSector.DoesNotExist:
                return None

            return IrrigationSectorDao.new_instance(sectors)

    @staticmethod
    def get_by_name(sector_name, farm_id):

        try:
            sectors = IrrigationSector.objects.get(label=sector_name, farm__id=farm_id)
        except IrrigationSector.DoesNotExist:
            return None

        return IrrigationSectorDao.new_instance(sectors)

    @staticmethod
    def get_all(serializable):

        s = IrrigationSector.objects.all()
        if serializable is True:
            seria = IrrigationSectorSerializer(s, many=True)
            return seria.data
        else:
            sectors = list()

            for i in s:
                sectors.append(IrrigationSectorDao.new_instance(i))

            return sectors

    @staticmethod
    def get_all_by_farm(farm_id, serialize):

        s = IrrigationSector.objects.filter(farm__id=farm_id)
        if serialize is False:
            sectors = list()
            for i in s:
                sectors.append(IrrigationSectorDao.new_instance(i))

            return sectors
        else:
            seria = IrrigationSectorSerializer(s, many=True)
            return seria.data

    @staticmethod
    def save_sensors(dict_sensors):

        # print(dict_sensors)

        for key, value in dict_sensors.items():
            names = key.split('.')
            farm_name = names[0]
            sector_name = names[1]
            sensor_name = names[2]

            farm = FarmDao.get_by_name(farm_name)

            if farm is None:
                farm = FarmDao.save(FarmModel(None, farm_name, None))

            if farm is not None:

                sector = IrrigationSectorDao.get_by_name(sector_name, farm.id)
                # print('found it')
                # print(sector)

                if sector is None:
                    sector = IrrigationSectorModel()
                    sector.farm = farm
                    sector.label = sector_name
                    sector = IrrigationSectorDao.save(sector)

                if sector is not None:
                    sector.sensors = value
                    IrrigationSectorDao.set_sensors(sensor_name, sector)

            # print(key, value)
            # print(json.dumps(value, ensure_ascii=False))
