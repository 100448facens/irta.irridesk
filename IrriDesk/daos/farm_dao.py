from django.contrib.auth.models import Group

from DBAPI.api.serializers import FarmSerializer
from DBAPI.models.farm import Farm
from IrriDesk.models.farm_model import FarmModel


class FarmDao:

    @staticmethod
    def new_instance(farm):
        if farm is None:
            return FarmModel(None, None, None)
        return FarmModel(farm.id, farm.label, farm.group)

    """ Create a new instance od farm in the database and return the object created """

    @staticmethod
    def save(farm_model):

        # get a default group in the database
        grp = farm_model.group
        if farm_model.group is None:
            grp = Group.objects.filter()[:1].get()

        f = Farm.create(label=farm_model.label, group=grp)
        return FarmDao.new_instance(f)

    @staticmethod
    def update(farm_model):
        f = Farm.objects.get(id=farm_model.id)
        f.label = farm_model.label
        f.group = farm_model.group
        # f.sectors = self.sectors
        f.save(update_fields=['label', 'group', 'sectors'])

        return FarmDao.new_instance(f)

    """ returns the number of objects deleted and a dictionary with the number of deletions per object type. """

    @staticmethod
    def delete(farm_model):
        f = Farm.objects.get(id=farm_model.id)
        return f.delete()

    @staticmethod
    def get(farm_id):

        try:
            farm = Farm.objects.get(id=farm_id)
        except Farm.DoesNotExist:
            return None

        return FarmDao.new_instance(farm)

    @staticmethod
    def get_by_name(farm_name):

        try:
            farm = Farm.objects.get(label=farm_name)
        except Farm.DoesNotExist:
            return None

        return FarmDao.new_instance(farm)

    @staticmethod
    def get_all(serialize):

        objects = Farm.objects.all()

        if serialize is False:
            
            farms = list()
            for f in objects:
                farms.append(FarmDao.new_instance(f))

            return farms

        else:
            seria = FarmSerializer(objects, many=True)
            return seria.data

    @staticmethod
    def get_all_serializable():
        seria = FarmSerializer(Farm.objects.all(), many=True)
        return seria.data

    @staticmethod
    def convert():
        farms = list()
        for f in Farm.objects.all():
            farms.append(FarmDao.new_instance(f))

        return farms
