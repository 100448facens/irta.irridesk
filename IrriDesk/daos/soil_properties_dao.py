import datetime

from DBAPI.api.serializers import SoilTypeSerializer
from DBAPI.models import SoilProperties
from DBAPI.models.crop_type import CropType
from DBAPI.models.soil_type import SoilType
from IrriDesk.models.soil_properties_model import SoilPropertiesModel


class SoilPropertiesDao:
    id = ""

    crop_type = ""
    crop = ""
    crop_options = ""

    distance_trees = 0.0
    distance_lines = 0.0
    plantation_year = 0

    tree_width = 0.0
    tree_depth = 0.0
    tree_height = 0.0

    start_vegetative_period = datetime
    flowering_date = datetime
    start_harvest = datetime
    end_harvest = datetime

    # TODO: ask to Jaume about this dictionary
    # additional_properties = list()

    @staticmethod
    def new_instance(soil, is_type):
        s = SoilPropertiesModel()
        s.id = soil.id
        s.soil_type = soil.soil_type
        s.swc_fc = soil.swc_fc
        s.swc_wp = soil.swc_wp

        if is_type is not True:
            s.soil_depth = soil.soil_depth
            s.stone_content = soil.stone_content

        return s

    @staticmethod
    def save(self):
        """ Create a new instance od farm in the database and return the object created """
        # f = SoilProperties.create(crop_type=self.crop_type,
        #                           crop=self.crop,
        #                           crop_options=self.crop_options,
        #                           distance_trees=self.distance_trees,
        #                           distance_lines=self.distance_lines,
        #                           plantation_year=self.plantation_year,
        #                           tree_width=self.tree_width,
        #                           tree_height=self.tree_height,
        #                           tree_depth=self.tree_depth,
        #                           start_vegetative_period=self.start_vegetative_period,
        #                           start_harvest=self.start_harvest,
        #                           flowering_date=self.flowering_date,
        #                           end_harvest=self.end_harvest)
        # return self.new_instance(f)
        return None

    @staticmethod
    def save_soil_type(self):

        SoilType.create(soil_type=self.soil_type, swc_wp=self.swc_wp, swc_fc=self.swc_fc)

    @staticmethod
    def update():
        return None

    @staticmethod
    def delete(self):
        """ returns the number of objects deleted and a dictionary with the number of deletions per object type. """
        f = SoilProperties.objects.get(id=self.id)
        return f.delete()

    @staticmethod
    def get_type(crop_type_id, serialize):
        obj = CropType.objects.get(crop_type_id)

        if serialize is False:
            return SoilPropertiesDao.new_instance(obj, True)
        else:
            seria = SoilTypeSerializer(obj, many=True)
            return seria.data

    @staticmethod
    def get_all_type(serialize):
        objects = SoilType.objects.all()

        if serialize is False:

            types = list()
            for f in objects:
                types.append(SoilPropertiesDao.new_instance(f, True))

            return types

        else:
            seria = SoilTypeSerializer(objects, many=True)
            return seria.data
