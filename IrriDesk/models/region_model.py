from DBAPI.models.region import Region
from IrriDesk.models.plot_model import PlotModel


class RegionModel(Region):
    name = ""
    plots = PlotModel
