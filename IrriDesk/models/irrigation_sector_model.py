class IrrigationSectorModel:
    id = None
    label = None
    farm = None  # FarmModel
    crop = None  # CropPropertiesModel
    soil = None  # SoilPropertiesModel
    irrigation_setup = None  # IrrigationSystemPropertiesModel
    gis_features = ""  # GisFeature
    sensors = list()  # list of SensorModel
    sensors_data = list()  # a list of SensorDataModel

    def __str__(self):
        return 'id={} label={} farm={} gis_features={} sensors={}'.\
            format(self.id, self.label, self.farm, self.gis_features, self.sensors)
