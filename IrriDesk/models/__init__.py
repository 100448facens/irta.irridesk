"""
this package (models) represent the system objects, not represent database objects


from IrriDesk.models.farm_model import *
from IrriDesk.models.irrigation_sector_model import *
from IrriDesk.models.irrigation_system_properties_model import *
from IrriDesk.models.soil_properties_model import *
from IrriDesk.models.crop_properties_model import *
from IrriDesk.models.plot_model import *
from IrriDesk.models.region_model import *
from IrriDesk.models.sensor_model import *


"""