class SensorModel:
    id = None
    label = ""
    series = list()  # a list of SensorSeriesModel

    def __init__(self, s_id, label):
        self.id = s_id
        self.label = label

    def __str__(self):
        return 'id={} label={} series={}'.format(self.id, self.label, self.series)
