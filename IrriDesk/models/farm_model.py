from json import JSONEncoder

from DBAPI.models import Farm


class FarmModel:
    id = ""
    label = ""
    group = ""
    # sectors = list()

    # weather = weatherContext

    def __init__(self, farm_id, label, group):
        self.id = farm_id
        self.label = label
        self.group = group
        # self.sectors = sectors

    def __str__(self):
        return '{} {} {}'.format(self.id, self.label, self.group)

    class Meta:
        model = Farm