class SensorDataModel:
    id = ""
    acumm_irr = ""
    irr_day = ""
    eto = ""
    irr_ok = ""
    plan_ok = ""
    sensor_ok = ""
    all_ok = ""
    date = ""

    def __init__(self, s_id, acumm_irr, irr_day, eto, irr_ok, plan_ok, sensor_ok, all_ok, date):
        self.id = s_id
        self.acumm_irr = acumm_irr
        self.irr_day = irr_day
        self.eto = eto
        self.irr_ok = irr_ok
        self.plan_ok = plan_ok
        self.sensor_ok = sensor_ok
        self.all_ok = all_ok
        self.date = date

    def __str__(self):
        return 'id={} acumm_irr={} irr_day={} eto={} irr_ok={} plan_ok={} sensor_ok={} all_ok={} date={}'.format(
            self.id, self.acumm_irr, self.irr_day, self.eto, self.irr_ok, self.plan_ok, self.sensor_ok, self.all_ok,
            self.date)
