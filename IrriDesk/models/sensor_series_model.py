class SensorSeriesModel:
    id = None
    init_at = None
    end_at = None
    data = ""  # json object

    def __init__(self, s_id, init_at, end_at, data):
        self.id = s_id
        self.init_at = init_at
        self.end_at = end_at
        self.data = data

    def __str__(self):
        return 'id={} init_at={} end_at={} data={}'.format(self.id, self.init_at, self.end_at, self.data)
