import datetime


class CropPropertiesModel:
    id = ""

    # crop_type = ""
    # crop = ""
    # crop_options = ""
    crop = None

    distance_trees = 0.0
    distance_lines = 0.0
    plantation_year = 0

    tree_width = 0.0
    tree_depth = 0.0
    tree_height = 0.0

    start_vegetative_perior = datetime
    flowering_date = datetime
    start_harvest = datetime
    end_harvest = datetime

    # TODO: ask to Jaume about this dictionary
    additional_properties = list()

    def __init__(self) -> None:
        super().__init__()





