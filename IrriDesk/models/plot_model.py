from DBAPI.models.plot import Plot
from IrriDesk.models.sensor_model import SensorModel


class PlotModel(Plot):
    name = ""
    sensors = SensorModel
