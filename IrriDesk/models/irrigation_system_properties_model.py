class IrrigationSystemPropertiesModel:
    id = ""

    drip_lines = 0.0
    dist_drippers = 0.0
    drip_flow = 0.0
    efficiency = 0.0
    wetted_diameter = 0.0
    max_allocation = 0.0

    # TODO: ask to Jaume about this dictionary
    additional_properties = list()
