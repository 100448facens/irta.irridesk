class CropTypeModel:
    id = ""

    crop_type = ""
    crop = ""
    crop_options = ""

    additional_properties = list()

    def __init__(self) -> None:
        super().__init__()

    def __str__(self):
        return 'id={} crop_type={} crop={} crop_options={}'.format(
            self.id, self.crop_type, self.crop, self.crop_options)
