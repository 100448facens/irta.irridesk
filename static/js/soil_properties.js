$(document).ready(function(){
    soil.initialize({
        select_soil: $('#select-soil-type'),
        field_swc_fc: $('#txt-water-capacity'),
        field_swc_wp: $('#txt-water-waiting')
    });
});

var soil = (function(){

    var selectSoil, fieldCapacity, fieldWaitingPoint;

    var initialize = function(options){
        selectSoil = options.select_soil;
        fieldCapacity = options.field_swc_fc;
        fieldWaitingPoint = options.field_swc_wp;

        selectSoil.change(showDetailsSoil);
    };

    function showDetailsSoil(e) {
        let $this = $(this);
        let selected = $this.find(':selected');

        let fc = selected.data('swc_fc');
        let wp = selected.data('swc_wp');

        fieldCapacity.val(fc);
        fieldWaitingPoint.val(wp);
    }

    return {
        initialize: initialize,
    }
})();