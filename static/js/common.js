// /** Common Events for all pages **/

/**
 * Show a pop for
 * @param message
 */
var popUpError = function (message, time) {

    if ($('#alert-message').size() > 0)
        $('#alert-message').alert('close');

    let html = '' +
        '<div id="alert-message" class="alert alert-danger fade in floating " style="width:450px;"> ' +
        '<button type="button" class="close" data-dismiss="alert" aria-label="Close"> ' +
        '<span aria-hidden="true">&times;</span> ' +
        '</button> ' +
        '<ul class="errors"> ' +
        '<li>' + message + '</li> ' +
        '</ul> ' +
        '</div>';
    $('body', document).append(html);

    hidePopUp();
};

var hidePopUp = function () {
    if ($('.floating').size()) {
        setTimeout(function () {
            //$('.floating').fadeOut('slow', 'linear');
            $('.floating').alert('close');
        }, 10000);
    }
};

var iconLoading = function () {
    return '<center>' +
        '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>' +
        '<span class="sr-only">Loading...</span></center>';
};


/**
 *  To submit a form form  a modal
 *  if submit ok reload the page
 * @param event
 * @param form
 * @param button
 * @param callback - function on success of submit form, default reload page
 */
function alterSubmitForm(event, form, button, callback) {
    event.preventDefault();

    $(button).button('loading');
    if ($('#btn-cancel', form).length > 0)
        $('#btn-cancel', form).css({'display': 'none'});

    //data: new FormData($(form)[0]),

    $.ajax({
        url: form.attr('action'),
        type: form.attr('method'),
        data: form.serialize(),
        success: function (data) {
            button.button('reset');

            if (!data.hasOwnProperty('error')) {
                $('#modal').modal('hide');

                if (callback !== null && typeof callback === 'function')
                    callback();
                else
                    location.reload();
            } else {
                var html = '';

                if (!(Object.getPrototypeOf(data.error) === String.prototype)) {
                    for (var i in data.error) {

                        if ($('input[name=' + i + ']').size() > 0)
                            $('input[name=' + i + ']').parent().addClass('has-error has-feedback');

                        if ($('textarea[name=' + i + ']').size() > 0)
                            $('textarea[name=' + i + ']').parent().addClass('has-error has-feedback');

                        html += '<li>' + data.error[i] + '</li>';
                    }
                } else {
                    html += '<li>' + data.error + '</li>';
                }

                $('.errors', form).html(html);
                $('.errors', form).parent().fadeIn(200, 'linear');

                setTimeout(function () {
                    $('.errors', form).parent().fadeOut();
                    $('input', form).parent().removeClass('has-error has-feedback');
                    $('textarea', form).parent().removeClass('has-error has-feedback');
                }, 5000);
            }

            if ($('#btn-cancel', form).length > 0)
                $('#btn-cancel', form).css({'display': ''});
        },
        error: function (xmlHttpRequest) {
            showError(xmlHttpRequest['responseText']);
            if ($('#btn-cancel', form).length > 0)
                $('#btn-cancel', form).css({'display': ''});
        }
    });
}


/** used just for development
 * open new window with this message error to developers **/
var showError = function (message) {
    if (message != "" && message != undefined) {
        var myWindow = window.open("", "Error");
        myWindow.document.write(message);
    }
};

var cleanModal = function (modal) {
    modal.unbind('hidden.bs.modal');
    modal.find('.modal-title').text('');
    modal.find('.modal-body').html('');
    modal.find('.modal-footer').find('#btn-save').unbind('click');

};

var convertJSONToString = function (json) {

    let ret = JSON.stringify(json);
    ret = ret.replace(/"/g, "'");
    return ret;

};

/**
 *
 * @param array json with format
 *      [
 *          {
 *              coordinates: array with lat y lng
 *              name: string sector's name
 *              type: string (Point, Polygon)
 *          }
 *      ]
 *
 * @returns {{type: string, features: Array}}
 */
var convertJSONToGeoJSON = function (list) {

    //convert object json in array json to keep format geojson
    let features = [];
    $.each(list, function (i) {
        let coord = list[i].coordinates, arr = [];

        if (coord != "") {
            coord = JSON.parse(coord.replace(/'/g,'"'));

            $.each(coord, function (i) {
                arr.push(Object.values(coord[i]));
            });
        }

        let name = list[i].name;
        let type = list[i].type;

        features.push({
            type: name,
            properties: {},
            geometry: {
                type: type,
                coordinate: arr
            }
        });
    });

    /**
     * Reference to http://geojson.org/
     **/
    return {
        type: "FeatureCollection",
        features: features
    };
};