$(document).ready(function () {

    result.initialize({
        farm: $('#select-farm'),
        sector: $('#select-sector'),
        date: $('#irrigation-date'),
        input_eto: $('#input-eto'),
        input_daily: $('#input-daily-irr'),
        btn_recal: $('#btn-recalculate'),
        btn_save: $('#btn-save'),
        icon_irri_failed: $('#irrigation-failed'),
        icon_plan_failed: $('#plan-failed'),
        icon_sensor_failed: $('#sensor-failed'),
        traffic: $('#traffic'),
        graph_sector: $('#graph-sector'),
        graph_irrigation: $('#graph-irrigation'),
        canvas_sector: $('#chart-sector'),
        canvas_irri: $('#chart-irrigation'),
    });
});

var result = (function () {

    var selectFarm,
        selectSector,
        inputDate,
        inputEto,
        inputDaily,
        btnSave,
        btnRecalculate,
        iconIrriFailed,
        iconPlanFailed,
        iconSensorFailed,
        iconTraffic,
        graphSector,
        graphIrri,
        initial_dt,
        final_dt,
        canvasSector,
        canvasIrri
    ;

    function init(options) {

        selectFarm = options.farm;
        selectSector = options.sector;
        inputDate = options.date;
        inputEto = options.input_eto;
        inputDaily = options.input_daily;
        btnRecalculate = options.btn_recal;
        btnSave = options.btn_save;
        iconIrriFailed = options.icon_irri_failed;
        iconPlanFailed = options.icon_plan_failed;
        iconSensorFailed = options.icon_sensor_failed;
        iconTraffic = options.traffic;
        graphSector = options.graph_sector;
        graphIrri = options.graph_irrigation;
        canvasSector = options.canvas_sector;
        canvasIrri = options.canvas_irri;

        selectFarm.change(function () {
            if ($(this).val() != 0)
                loadSectors($(this).val())

        });

        selectSector.change(function () {
            if ($(this).val() != 0)
                inputDate.change();

        });

        inputDate.change(function(){
            let $this = $(this);
            let dt = moment($this.val(), 'YYYY-MM-DD');
            initial_dt = dt.clone().subtract(40,'d').format('YYYY-MM-DD');
            //final_dt = dt.clone().add(20,'d').format('YYYY-MM-DD');
            final_dt = dt.clone().format('YYYY-MM-DD');

            loadSensorData(initial_dt, final_dt);
            loadSensorSeries(final_dt);
        });

        //loadSensorData();
        //loadSensorSeries();

        $('[data-toggle="tooltip"]').tooltip()
    }

    function loadSectors(farm_id) {

        function renderSectors(list) {
            console.log(list);
            selectSector.html('<option value="0">Select one</option>');
            for (let i in list) {
                let sector = list[i];
                selectSector.append('<option value="' + sector.id + '">' + sector.label + '</option>');
            }
        }

        $.ajax({
            url: '/irrigation/get/sector/' + farm_id,
            type: 'get',
            success: function (data) {
                renderSectors(data.list)
            }
        });

    };

    function loadSensorData(date_i, date_e) {
        let farm_id = selectFarm.val();
        let sector_id = selectSector.val();
        //let date_i = '2018-03-28';
        //let date_e = '2018-09-04';

        $.ajax({
            url: '/irrigation/irrigation/result/sensor_data/' + sector_id + '/' + date_i + '/' + date_e,
            type: 'get',
            success: function (data) {

                console.log(data);
                let datas = data.data;


                let data_eto = [], data_irr = [], labels = [];

                for (let i in datas) {
                    let d = datas[i];

                    data_irr.push(d.irr_day);
                    data_eto.push(d.eto);
                    let date = moment(d.date, 'YYYY/MM/DD hh:mm:ss');
                    //labels.push(date);
                    labels.push(moment(date, 'YYYY-MM-DD'));
                }


                printGraphEto(labels, {
                    label: 'ETo',
                    data: data_eto,

                    // Changes this dataset to become a line
                    type: 'line',
                    fill: false,
                    borderColor: 'orange'
                }, {
                    label: 'Irrigation',
                    data: data_irr,
                    type: 'bar',
                    fill: true,
                    backgroundColor: 'blue'
                })

                updateLegend(datas[datas.length - 1]);
            }
        });

    }

    function updateLegend(obj_series) {

        inputDaily.val(obj_series.irr_day);
        inputEto.val(obj_series.eto);

        if(obj_series.plan_ok < 0.7)
            iconPlanFailed.show();
        else
            iconPlanFailed.hide();

        if(obj_series.sensor_ok < 0.7)
            iconSensorFailed.show();
        else
            iconSensorFailed.hide();

        if(obj_series.irr_ok < 0.7)
            iconIrriFailed.show();
        else
            iconIrriFailed.hide();

        iconTraffic.removeClass('text-success text-danger text-warning text-muted');
        if(obj_series.all_ok < 0.5)
            iconTraffic.addClass('text-danger')
        else if(obj_series.all_ok < 0.7)
            iconTraffic.addClass('text-warning')
        else
            iconTraffic.addClass('text-success')
    }

    function loadSensorSeries(date) {
        let farm_id = selectFarm.val();
        let sector_id = selectSector.val();
        //let date = '2018-07-28';
        var dateFormat = 'DD/MM/YYYY hh:mm';
        var color = ['red', 'blue', 'green', 'yellow'];

        $.ajax({
            url: '/irrigation/irrigation/result/sensor_series/' + farm_id + '/' + sector_id + '/' + date,
            type: 'get',
            success: function (data) {

                let sensors = data.sensor;
                let series = data.series;

                var s_data = [], s_labels = [];
                for (let i in series) {
                    let serie = series[i];

                    var all_series = JSON.parse(serie[0].data);

                    let list = [], label = [];
                    for (var j = 0; j < all_series.length; j++) {
                        var s = all_series[j];


                        var date = moment(s.date, dateFormat);
                        var f_date = date.format('YYYY-MM-DD');

                        if(date.isBefore(final_dt) && date.isAfter(initial_dt)) {
                            label[f_date] = f_date;
                            list[f_date] = s.value;
                        }
                    }

                    if (s_labels.length == 0) {

                        label.sort(function (a, b) {
                            let dta = moment(a);
                            let dtb = moment(b);
                            return dta.isBefore(dtb);
                        });

                        for (var k in label) s_labels.push(moment(label[k], 'YYYY-MM-DD'));
                    }

                    let aux = [];
                    for (var k in label) aux.push(list[k]);
                    s_data.push(aux);
                }

                var s_dataset = [];
                for (var i = 0; i < sensors.length; i++) {
                    let sensor = sensors[i];

                    s_dataset.push({
                        label: sensor.label,
                        data: s_data[i],
                        type: 'line',
                        pointRadius: 0,
                        fill: false,
                        lineTension: 0,
                        borderWidth: 2,
                        borderColor: color[i]
                    });
                }

                printGraph(s_labels, s_dataset)
            }
        });

    }

    function printGraph(labels, dataset) {
        canvasSector.remove(); // this is my <canvas> element
        graphSector.append('<canvas id="chart-sector" width="100%" height="100%"></canvas>');
        canvasSector = $('#chart-sector');
        var ctx = $("#chart-sector");

        // for(var i in labels) console.log(labels[i].format('DD MM YYYY'));
        var cfg = {
            type: 'bar',
            data: {
                labels: labels,
                datasets: dataset
            },
            options: {
                legend: {
                    // position: 'bottom',
                },
                responsive: true,
                maintainAspectRatio: false,
                scales: {
                    xAxes: [{
                        display: false,
                        type: 'time',
                        distribution: 'series',
                        ticks: {
                            source: 'labels'
                        },
                        time: {
                            unit: 'month'
                        }
                    }],
                    yAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: 'Soil Water Content, m3/m3'
                        }
                    }]
                }
            }
        };
        var chart = new Chart(ctx, cfg);
    }

    function printGraphEto(labels, dataset_day, dataset_eto) {
        canvasIrri.remove(); // this is my <canvas> element
        graphIrri.append('<canvas id="chart-irrigation" width="100%" height="100%"></canvas>');
        canvasIrri = $('#chart-irrigation');

        var ctx = canvasIrri;


        var mixedChart = new Chart(ctx, {
            type: 'bar',
            data: {
                datasets: [dataset_day, dataset_eto],
                labels: labels
            },
            options: {
                legend: {
                    //position: 'bottom',
                },
                responsive: true,
                maintainAspectRatio: false,
                scales: {
                    xAxes: [{
                        type: 'time',
                        time: {
                            unit: 'day',
                            displayFormats: {
                                day: 'DD-MM-YYYY'
                            }
                        },
                        distribution: 'series',
                        ticks: {
                            source: 'labels'
                        },
                    }],
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        },
                        scaleLabel: {
                            display: true,
                            labelString: 'Irrigation and ETo, mm'
                        }
                    }]
                }
            }
        });
    }


    return {
        initialize: init,
    }
})();
