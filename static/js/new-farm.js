$(document).ready(function () {
    newFarm.initialize({
        farm: $('#select-farm'),
        sector: $('#select-sector'),
        btn_add_farm: $('#add-farm'),
        btn_add_sector: $('#add-sector'),
        panel_add: $('#panel-add'),
        add_region: $('#btn-create-region'),
        feature_name: $('#feature-name'),
        table_features: $('#table-features'),
        token_csrf: $('input[name=csrfmiddlewaretoken]'),
        radio_type: $('input[name=rbt_type]'),
    });
});

var newFarm = (function () {

    var selectFarm = null,
        selectSector = null,
        btnAddFarm = null,
        btnAddSector = null,
        panelAdd = null,
        regionAdd = null,
        featureName = null,
        featuresTable = null,
        listMarkers = [],
        tokenCsrf,
        currentPolyline,
        map,
        radioType,
        poligonsArray = [],
        typeSelected = 'polygon';
    ;

    var init = function (options) {
        selectFarm = options.farm;
        selectSector = options.sector;
        btnAddFarm = options.btn_add_farm;
        btnAddSector = options.btn_add_sector;
        panelAdd = options.panel_add;
        regionAdd = options.add_region;
        featureName = options.feature_name;
        featuresTable = options.table_features,
            radioType = options.radio_type,
            tokenCsrf = options.token_csrf;

        selectFarm.change(function () {
            if ($(this).val() != 0)
                loadSectors($(this).val())
            cleanMap();
            featuresTable.find('tbody').html('');
        });

        selectSector.change(function () {
            if ($(this).val() != 0)
                loadFeatures($(this).val())
            cleanMap();
            featuresTable.find('tbody').html('');
        });

        btnAddFarm.click(function () {
            showFormNewFarm();
        });

        btnAddSector.click(function () {
            showFormNewSector();
        });

        regionAdd.click(function () {
            createPolyline();
        });

        radioType.change(function () {
            let action = $(this).data('action');
            //changeClickInMap(action);
            typeSelected = action;

            for (var i in listMarkers)
                listMarkers[i].setMap(null);
            listMarkers = [];

            createInstancePolyline();

        });
    };

    function showFormNewFarm() {
        let modal = $('#modal');

        modal.find('.modal-title').text('New Farm');
        modal.unbind('shown.bs.modal');
        modal.on('shown.bs.modal', function () {

            $.ajax({
                url: '/irrigation/get/html/farm',
                type: 'get',
                success: function (data) {

                    // console.log(data);

                    // panelAdd.html(data);

                    modal.find('.modal-body').html(data);

                    let btn_save = modal.find('.modal-footer').find('#btn-save');
                    let form = modal.find('.modal-body').find('form');
                    form.submit(function (event) {
                        alterSubmitForm(event, form, btn_save, function () {

                            loadFarms();

                            modal.modal('hide');
                        })
                    });

                    btn_save.click(function () {
                        form.submit();
                    });
                }
            });

        });

        modal.on('hidden.bs.modal', function () {
            cleanModal(modal)
        });

        modal.modal('show');
    }

    function showFormNewSector() {
        let modal = $('#modal');
        let farm_name = selectFarm.find(':selected').html();
        let farm_id = selectFarm.val();

        if (farm_id == undefined || farm_id == 0) {
            alert('Choose one farm');
            return;
        }

        modal.find('.modal-title').text('New Sector to ' + farm_name);
        modal.unbind('shown.bs.modal');
        modal.on('shown.bs.modal', function () {

            $.ajax({
                url: '/irrigation/get/html/sector/' + farm_id,
                type: 'get',
                success: function (data) {

                    // console.log(data);

                    // panelAdd.html(data);

                    modal.find('.modal-body').html(data);

                    let btn_save = modal.find('.modal-footer').find('#btn-save');
                    let form = modal.find('.modal-body').find('form');
                    form.submit(function (event) {
                        alterSubmitForm(event, form, btn_save, function () {

                            loadSectors(farm_id);

                            modal.modal('hide');
                        })
                    });

                    btn_save.click(function () {
                        form.submit();
                    });
                }
            });

        });

        modal.on('hidden.bs.modal', function () {
            cleanModal(modal)
        });

        modal.modal('show');
    }

    var loadSectors = function (farm_id) {

        function renderSectors(list) {
            console.log(list);
            selectSector.html('<option value="0">Select one</option>');
            for (let i in list) {
                let sector = list[i];
                selectSector.append('<option value="' + sector.id + '">' + sector.label + '</option>');
            }
        }

        $.ajax({
            url: '/irrigation/get/sector/' + farm_id,
            type: 'get',
            success: function (data) {
                renderSectors(data.list)
            }
        });

    };

    function loadFarms() {

        function render(list) {
            console.log(list);
            selectFarm.html('');
            for (let i in list) {
                let f = list[i];
                selectFarm.append('<option value="' + f.id + '">' + f.label + '</option>');
            }
        }

        $.ajax({
            url: '/irrigation/get/farms/',
            type: 'get',
            success: function (data) {
                render(data.list)
            }
        });
    }

    function validateFields() {
        if (selectFarm.val() == 0) {
            alert('Choose the farm');
            return false;
        }

        if (selectSector.val() == 0) {
            alert('Choose the sector');
            return false;
        }

        if (listMarkers.length <= 2 && typeSelected === 'polygon') {
            alert('Select 3 or more points');
            return false;
        }

        if (featureName.val() == "" || featureName.val() == undefined) {
            alert('Missing feature name');
            return false;
        }

        return true;

    }

    function getHtmlFeatures(name, features, type) {

        let pos = featuresTable.find('tbody > tr').length;

        return '' +
            '<tr data-features="' + features + '" data-label="' + name + '" data-pos="' + pos + '" data-type="' + type + '">' +
            '   <td>' +
            '       <span data-action="focus"><i class="fas fa-map-pin"></i></span>' +
            '   </td>' +
            '   <td><small>' + name + '</small></td>' +
            '   <td></td>' +
            '   <td>' +
            '       <span data-action="show" data-show="1"><i class="far fa-eye"></i></span>' +
            '       <span data-action="delete"><i class="fas fa-trash"></i></span>' +
            '   </td>' +
            '</tr>';

    };

    function createLineFeature(name, features, type) {

        featuresTable.append(getHtmlFeatures(name, convertJSONToString(features), type))

        loadEventsFeatures();
        saveFeature();
    }

    function saveFeature() {

        let data = [];
        featuresTable.find('tbody > tr').each(function () {
            let features = $(this).data('features');
            let label = $(this).data('label');
            let type = $(this).data('type');

            data.push({coordinates: features, name: label, type: type})
        });

        data = convertJSONToGeoJSON(data);

        //featuresTable.find('tbody').html('');

        console.log("save", data);

        $.ajax({
            url: '/irrigation/save/features',
            type: 'post',
            data: {
                features: JSON.stringify(data),
                farm_id: selectFarm.val(),
                sector_id: selectSector.val(),
                csrfmiddlewaretoken: tokenCsrf.val()
            },
            success: function (data) {
                //loadFeatures($(selectSector).val());
            }
        });
    }

    function loadFeatures(sector_id) {

        function render(gis_features) {

            if (gis_features == "") return;

            console.log(gis_features)
            let list = JSON.parse(gis_features);

            featuresTable.find('tbody').html('');
            for (let i in list.features) {

                let f = list.features[i];
                console.log(f);
                let coord = f.geometry.coordinate;
                let name = f.type;
                let type = f.geometry.type;

                coord = convertArrayCoordinatesInJsonObject(coord)

                if (type === 'Point') {
                    drawPointInMap(coord, true);
                } else {
                    drawPolygonInMap(coord, true);
                }

                featuresTable.append(getHtmlFeatures(name, convertJSONToString(coord), type))
            }
        }

        $.ajax({
            url: '/irrigation/get/features/' + sector_id,
            type: 'get',
            success: function (data) {
                console.log("jose", data);

                data = data[0];

                //object data with parameters: crop_id, farm_id, gis_features, id, irrigation_setup_id, label, soil_id
                render(data.gis_features)

                loadEventsFeatures();
            }
        });
    }

    function loadEventsFeatures() {
        let trs = featuresTable.find('tbody > tr');
        trs.find('[data-action=show]').unbind('click').click(function () {
            let pos = $(this).parents('tr').data('pos');
            let isShow = $(this).data('show') == '1' ? true : false;

            if (poligonsArray[pos] != undefined) {
                if (isShow) {
                    $(this).data('show', 0);
                    $(this).html('<i class="far fa-eye-slash"></i>');
                    $(this).addClass('text-muted');
                    poligonsArray[pos].setMap(null);
                } else {
                    $(this).data('show', 1);
                    $(this).html('<i class="far fa-eye"></i>');
                    $(this).removeClass('text-muted');

                    poligonsArray[pos].setMap(map);
                }

            }
        });

        trs.find('[data-action=delete]').click(function () {
            let tr = $(this).parents('tr');
            let pos = tr.data('pos');
            tr.remove();
            poligonsArray[pos].setMap(null);

            saveFeature();

        });

        trs.find('[data-action=focus]').click(function () {
            let tr = $(this).parents('tr');
            let pos = tr.data('pos');
            let type = tr.data('type');

            let p = poligonsArray[pos];
            let coor;
            if (type === 'Polygon') {
                coor = p.getPath().getArray();
                map.setCenter(coor[0]);
            } else {
                coor = p.getPosition();
                map.setCenter(coor);
            }

            map.setZoom(17);

        });
    }

    function drawPolygonInMap(array_coordinates, is_new) {
        console.log('array_coordinates', array_coordinates)

        let fill_color = 'blue', stroke_color = 'blue', stroke_w = 2.5;
        if (is_new == true) {
            fill_color = 'yellow';
            stroke_color = 'red';
            stroke_w = 0.5;
        }

        var flightPath = new google.maps.Polygon({
            path: array_coordinates,
            geodesic: true,
            strokeColor: stroke_color,
            fillColor: fill_color,
            fillOpacity: 0.35,
            //strokeOpacity: 1.0,
            strokeWeight: stroke_w
        });

        flightPath.setMap(map);
        flightPath.addListener('click', function (e) {
            if (typeSelected === 'point') {

                if (listMarkers.length > 0) {
                    poligonsArray[poligonsArray.length - 1].setMap(null);
                    poligonsArray.pop();
                    listMarkers[0].setMap(null);
                    listMarkers.pop();
                }

                drawPointInMap([e.latLng]);
                listMarkers.push(poligonsArray[poligonsArray.length - 1]);
            }
        });

        poligonsArray.push(flightPath);
    }

    function drawPointInMap(array_coordinates, is_new) {
        console.log('point: array_coordinates', array_coordinates)

        let fill_color = 'blue', stroke_color = 'blue', stroke_w = 2.5;
        if (is_new == true) {
            fill_color = 'yellow';
            stroke_color = 'red';
            stroke_w = 0.5;
        }

        var flightPath = new google.maps.Marker({
            position: array_coordinates[0],
            map: map,
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                fillColor: fill_color,
                fillOpacity: 1,
                strokeColor: stroke_color,
                strokeWeight: stroke_w,
                scale: 6
            }
        });

        flightPath.setMap(map);

        poligonsArray.push(flightPath);
    }


    function convertArrayCoordinatesInJsonObject(array) {

        let arr = []
        for (var i in array) {
            let pos = array[i];
            let lat = pos[0];
            let lng = pos[1];
            arr.push({lat, lng})
        }

        return arr;
    }


    function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: 41.618296, lng: 0.626866},
            zoom: 12,
            mapTypeId: 'satellite',
            mapTypeControl: false,
            scaleControl: false,
            streetViewControl: false,
            rotateControl: false,
        });

        google.maps.event.addListener(map, 'click', function (e) {

            if (typeSelected === 'point') {
                eventToDrawPoint(e);
            } else {
                eventToDrawPolygon(e);
            }

        });

        // poly = new google.maps.Polyline({
        //     strokeColor: resultColor,
        //     strokeOpacity: 1.0,
        //     strokeWeight: 1.0,
        // });
        // poly.setMap(map);

        createInstancePolyline();
    }

    function eventToDrawPoint(e) {
        var resultPath = google.maps.SymbolPath.CIRCLE;


        if (listMarkers.length == 0) {
            listMarkers.push(new google.maps.Marker({
                position: e.latLng,
                map: map,
                icon: {
                    path: resultPath,
                    fillColor: 'red',
                    fillOpacity: 1,
                    strokeColor: 'white',
                    strokeWeight: .5,
                    scale: 6
                }
            }));
        } else {
            listMarkers[0].setPosition(e.latLng)
        }

    }

    function eventToDrawPolygon(e) {
        var path = currentPolyline.getPath();
        path.push(e.latLng);

        var resultPath = google.maps.SymbolPath.CIRCLE;

        listMarkers.push(new google.maps.Marker({
            position: e.latLng,
            map: map,
            title: '#' + path.getLength(),
            icon: {
                path: resultPath,
                fillColor: 'black',
                fillOpacity: .2,
                strokeColor: 'white',
                strokeWeight: .5,
                scale: 6
            }
        }));

        if (listMarkers.length == 1) {
            //event to close the polygon
            listMarkers[0].addListener('click', function () {
                if (listMarkers.length >= 3) {
                    let flightPlanCoordinates = [];
                    for (var i in listMarkers) {
                        var mark = listMarkers[i];
                        let lat = mark.getPosition().lat();
                        let lng = mark.getPosition().lng();
                        let obj = {lat, lng}
                        flightPlanCoordinates.push(obj);

                        //mark.setMap(null);
                    }

                    drawPolygonInMap(flightPlanCoordinates, false);
                }
            });
        }
    }

    function createPolyline() {

        if (!validateFields()) {
            return;
        }

        var flightPlanCoordinates;
        if (typeSelected === 'polygon') {
            flightPlanCoordinates = poligonsArray[poligonsArray.length - 1].getPath().getArray();
        } else {
            flightPlanCoordinates = [listMarkers[0].getPosition()];
        }


        /*for (var i in listMarkers) {
            var mark = listMarkers[i];
            let lat = mark.getPosition().lat();
            let lng = mark.getPosition().lng();
            let obj = {lat, lng}
            flightPlanCoordinates.push(obj);

            //mark.setMap(null);
        }

        drawPolygonInMap(flightPlanCoordinates, true);*/

        for (var i in listMarkers)
            listMarkers[i].setMap(null);

        listMarkers = [];

        createInstancePolyline();
        createLineFeature(featureName.val(), flightPlanCoordinates, typeSelected === 'polygon' ? 'Polygon' : 'Point');
        featureName.val('')
    }

    function createInstancePolyline() {

        if (currentPolyline != null)
            currentPolyline.setMap(null);

        currentPolyline = new google.maps.Polyline({
            strokeColor: 'blue',
            strokeOpacity: 1.0,
            strokeWeight: 3
        });
        currentPolyline.setMap(map);

        // polylinesArray.push(p)

        // currentPolyline = p;
    }

    function cleanMap() {
        for (var i in poligonsArray) {
            poligonsArray[i].setMap(null);
        }
        poligonsArray = [];

        for (var i in listMarkers) {
            listMarkers[i].setMap(null)
        }

        listMarkers = [];

        createInstancePolyline();
    }

    return {
        initialize: init,
        initMap: initMap
    }
})();