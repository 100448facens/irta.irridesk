$(document).ready(function () {
    crop.initialize({
        select_group: $('#select-crop-group'),
        select_crop: $('#select-crop'),
        select_options: $('#select-crop-options'),
    });
});

var crop = (function () {

    var selectGroup,
        selectCrop,
        selectOption;

    var initialize = function (options) {
        selectCrop = options.select_crop;
        selectGroup = options.select_group;
        selectOption = options.select_options;

        selectGroup.change(function (e) {

            loadCrop($(this).val());
        });

        selectCrop.change(function (e) {

            loadCropOptions(selectGroup.val(), $(this).val());
        });
    };

    function loadCrop(crop_group) {

        var render = function (dict) {
            selectCrop.html('<option value="0">Choose one</option>');

            for (let i in dict) {
                selectCrop.append('<option value="' + i + '">' + i + '</option>');
            }
        };

        $.ajax({
            url: '/irrigation/get/crop/' + crop_group,
            type: 'get',
            success: function (data) {
                render(data);
            },
        })
    }

    function loadCropOptions(crop_group, crop) {

        var render = function (dict) {
            console.log(dict);
            selectOption.html('<option value="0">Choose one</option>');

            for (let i in dict) {
                selectOption.append('<option value="' + dict[i] + '">' + i + '</option>');
            }
        };

        $.ajax({
            url: '/irrigation/get/crop_option/' + crop_group + '/' + crop,
            type: 'get',
            success: function (data) {

                if (Object.keys(data).length == 0) {
                    alert(Object.keys(data).length);
                    selectOption.addClass('disabled').prop('disabled', true);
                } else {
                    selectOption.removeClass('disabled').prop('disabled', false);
                    render(data);
                }
            },
        })
    }

    return {
        initialize: initialize,
    }
})();