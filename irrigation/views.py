import os
from itertools import islice

from django.http import HttpResponseRedirect
from django.shortcuts import render
import openpyxl
from pandas import DataFrame
from gmplot import gmplot

from DBAPI.models.sensor import Sensor
from IrriDesk.daos import SensorDao, SensorDataModel, CropTypeModel
from IrriDesk.models.soil_properties_model import SoilPropertiesModel

from IrriDesk.views.irrigation_view import *

from datetime import datetime


def simple_upload(request):
    def parse_line(line):
        return line

    if request.method == 'POST':
        new_persons = request.FILES['myfile']
        wb = openpyxl.load_workbook(new_persons)

        # getting all sheets
        sheets = wb.sheetnames
        print(sheets)

        active_sheet = wb.active
        print(active_sheet)

        worksheet = wb['Hoja1']
        print(worksheet)

        excel_data = list()
        # iterating over the rows and
        # getting value from each cell in row
        for row in worksheet.iter_rows():
            row_data = list()
            parse_line(row_data)
            '''for cell in row:
                if cell.value is not None:
                    row_data.append(str(cell.value))'''

            excel_data.append(row_data)

        for row in excel_data:
            for cell in row:
                print(cell)

        data = worksheet.values
        cols = next(data)[1:]
        print("JOSE JOSE")
        print(cols)

        print('---0-0-0-0-0-0-0-0-0')

        '''for c in cols:
            res = c.split('.')
            print(res[0], res[1], res[2])

            fil = Region.objects.filter(name=res[0])
            if fil.count() <= 0:
                r = Region(name=res[0])
                r.save()

            fil = Plot.objects.filter(name=res[1])
            if fil.count() <= 0:
                p = Plot(name=res[1])
                p.save()

            fil = Sensor.objects.filter(name=res[2])
            if fil.count() <= 0:
                s = Sensor(name=res[2])
                s.save()

        print('---0-0-0-0-0-0-0-0-0')
'''
        data = list(data)
        print(data)

        for r in data:
            print(r[0])

        idx = [r[0] for r in data]
        print(idx)
        data = (islice(r, 1, None) for r in data)
        print("JOSE JOSE JOSE")
        print(data)
        df = DataFrame(data, index=idx, columns=cols)
        print(df)

        print('')
        print('')
        for index, row in df.iterrows():
            # print(index)
            for r in row:
                print(r)

            print('----------')

        print('********************')
        for row in df.itertuples(index=True, name='Pandas'):
            print(row)
            print(getattr(row, "Index"), getattr(row, "_1"))
            for r in row[1:]:  # start the for from position 1
                print(r)
        '''data = list(data)
        idx = [r[0] for r in data]
        data = (islice(r, 1, None) for r in data)
        df = DataFrame(data, index=idx, columns=cols)
'''
        return render(request, 'import.html')


def show_simple_upload(request):
    settings_dir = os.path.dirname(__file__)
    PROJECT_ROOT = os.path.abspath(os.path.dirname(settings_dir))
    FILES_FOLDER = os.path.join(PROJECT_ROOT, 'static/files')
    # file_ = open(os.path.join(XMLFILES_FOLDER, 'sensors.xlsx'))
    file_ = os.path.join(FILES_FOLDER, 'sensors.xlsx')
    wb = openpyxl.load_workbook(file_)

    # getting all sheets
    # sheets = wb.sheetnames
    # print(sheets)

    # active_sheet = wb.active
    # print(active_sheet)

    worksheet = wb['sensors']
    # print(worksheet)

    excel_data = list()
    # iterating over the rows and
    # getting value from each cell in row
    for row in worksheet.iter_rows():
        # row_data = list()
        # parse_line(row_data)
        '''for cell in row:
            if cell.value is not None:
                row_data.append(str(cell.value))'''

        excel_data.append(row)

        '''for row in excel_data:
        for cell in row:
            print(cell) '''

        data = worksheet.values
    cols = next(data)[1:]
    '''print("JOSE JOSE")
    print(cols)

    print('---0-0-0-0-0-0-0-0-0')'''

    '''for c in cols:
        res = c.split('.')
        print(res[0], res[1], res[2])

        fil = Region.objects.filter(name=res[0])
        if fil.count() <= 0:
            r = Region(name=res[0])
            r.save()

        fil = Plot.objects.filter(name=res[1])
        if fil.count() <= 0:
            p = Plot(name=res[1])
            p.save()

        fil = Sensor.objects.filter(name=res[2])
        if fil.count() <= 0:
            s = Sensor(name=res[2])
            s.save()

    print('---0-0-0-0-0-0-0-0-0')
'''
    data = list(data)
    '''print(data)

    for r in data:
        print(r[0])

    idx = [r[0] for r in data]
    print(idx)
    data = (islice(r, 1, None) for r in data)
    print("JOSE JOSE JOSE")
    print(data)'''

    idx = [r[0] for r in data]
    # print(idx)
    # print(cols)

    dictOfWords = {i: list() for i in cols}
    # print(dictOfWords)

    data = (islice(r, 1, None) for r in data)
    df = DataFrame(data, index=idx, columns=cols)
    # print(df)

    # print('')
    # print('')
    '''for index, row in df.iterrows():
        # print(index)
        for r in row:
            print(r)

        print('----------')'''

    # print('********************')
    for row in df.itertuples(index=True, name='Pandas'):
        # print(row)
        dt = getattr(row, "Index").replace("'", "")
        vl1 = getattr(row, "_1")
        vl2 = getattr(row, "_2")
        vl3 = getattr(row, "_3")
        vl4 = getattr(row, "_4")
        vl5 = getattr(row, "_5")
        vl6 = getattr(row, "_6")
        vl7 = getattr(row, "_7")
        vl8 = getattr(row, "_8")
        vl9 = getattr(row, "_9")
        vl10 = getattr(row, "_10")
        vl11 = getattr(row, "_11")
        vl12 = getattr(row, "_12")

        # print(dt, vl1, vl2, vl3, vl4, vl5, vl6, vl7, vl8, vl9, vl10, vl11, vl12)

        dictOfWords.get(cols[0]).append({'date': dt, 'value': vl1})
        dictOfWords.get(cols[1]).append({'date': dt, 'value': vl2})
        dictOfWords.get(cols[2]).append({'date': dt, 'value': vl3})
        dictOfWords.get(cols[3]).append({'date': dt, 'value': vl4})
        dictOfWords.get(cols[4]).append({'date': dt, 'value': vl5})
        dictOfWords.get(cols[5]).append({'date': dt, 'value': vl6})
        dictOfWords.get(cols[6]).append({'date': dt, 'value': vl7})
        dictOfWords.get(cols[7]).append({'date': dt, 'value': vl8})
        dictOfWords.get(cols[8]).append({'date': dt, 'value': vl9})
        dictOfWords.get(cols[9]).append({'date': dt, 'value': vl10})
        dictOfWords.get(cols[10]).append({'date': dt, 'value': vl11})
        dictOfWords.get(cols[11]).append({'date': dt, 'value': vl12})

        # for r in row[1:]:  # start the for from position 1
        #     print(r)

    # print(dictOfWords)

    IrrigationSectorDao.save_sensors(dictOfWords)

    # return render(request, 'import.html')
    return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))


def import_data_sensor(request):
    def include_date(sec, d):
        SensorDao.set_data(sec,
                           SensorDataModel(None, d.acumm_irr, d.irr_day, d.eto, d.irr_ok, d.plan_ok,
                                           d.sensor_ok, d.all_ok, d.date))

    def iterate_excel(excel, name, farm_name, sector_name):
        farm = FarmDao.get_by_name(farm_name)
        irri_sector = IrrigationSectorDao.get_by_name(sector_name, farm.id)

        if farm is not None and irri_sector is not None:

            worksheet = excel[name]
            data = worksheet.values

            data = list(data)[1:]

            for r in data:
                date = r[0]
                acumm_irr = r[1]
                irr_day = r[2]
                eto = r[3]
                irr_ok = r[4]
                plan_ok = r[5]
                sensor_ok = r[6]
                all_ok = r[7]

                # date_time_obj = datetime.datetime.strptime(date, '%Y-%m-%d')
                dt = datetime.strptime(date, "%d/%m/%Y").date()

                sm = SensorDataModel(None, acumm_irr, irr_day, eto, irr_ok, plan_ok, sensor_ok, all_ok, dt)
                include_date(irri_sector, sm)

    settings_dir = os.path.dirname(__file__)
    PROJECT_ROOT = os.path.abspath(os.path.dirname(settings_dir))
    FILES_FOLDER = os.path.join(PROJECT_ROOT, 'static/files')
    # file_ = open(os.path.join(XMLFILES_FOLDER, 'sensors.xlsx'))
    mep1_ = os.path.join(FILES_FOLDER, 'Menarguens.parcela1.xlsx')
    mep2_ = os.path.join(FILES_FOLDER, 'Menarguens.parcela2.xlsx')
    mos1_ = os.path.join(FILES_FOLDER, 'Mollerussa.sector1.xlsx')
    mos2_ = os.path.join(FILES_FOLDER, 'Mollerussa.sector2.xlsx')
    mep1 = openpyxl.load_workbook(mep1_)
    mep2 = openpyxl.load_workbook(mep2_)
    mos1 = openpyxl.load_workbook(mos1_)
    mos2 = openpyxl.load_workbook(mos2_)

    # include_date(sensor, list())

    iterate_excel(mep1, "Menarguens.parcela1", "Menarguens", "parcela1")
    iterate_excel(mep2, "Menarguens.parcela2", "Menarguens", "parcela2")
    iterate_excel(mos1, "Mollerussa.sector1", "Mollerussa", "sector1")
    iterate_excel(mos2, "Mollerussa.sector2", "Mollerussa", "sector2")

    return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))


def import_crop_soil_properties(request):
    settings_dir = os.path.dirname(__file__)
    PROJECT_ROOT = os.path.abspath(os.path.dirname(settings_dir))
    FILES_FOLDER = os.path.join(PROJECT_ROOT, 'static/files')
    file_ = os.path.join(FILES_FOLDER, 'Soil&CropOptions.xlsx')

    file = openpyxl.load_workbook(file_)

    worksheetS = file['SoilProperties']
    worksheetC = file['crops']
    dataS = worksheetS.values
    dataC = worksheetC.values

    for r in list(dataS)[2:]:
        s = SoilPropertiesModel()
        s.soil_type = r[0]
        s.swc_fc = r[1]
        s.swc_wp = r[2]
        SoilPropertiesDao.save_soil_type(s)

    for r in list(dataC)[1:]:
        s = CropTypeModel()
        s.crop_type = r[0]
        s.crop = r[1]
        s.crop_options = r[2]
        CropPropertiesDao.save_crop_type(s)

    print("*****************************************")

    return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
