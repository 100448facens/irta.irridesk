from django.urls import path
from . import views

urlpatterns = [
    path('', views.main, name='main_screen'),
    path('init/', views.show_weather_data, name='weather_data'),
    path('soil/', views.show_soil_definition, name='soil_definition'),
    path('crop/', views.show_crop_definition, name='crop_definition'),
    path('irrigation/', views.show_irrigation_setup, name='irrigation_setup'),
    path('irrigation/result', views.show_result, name='show_result'),
    path('irrigation/result/sensor_data/<sector_id>/<date_i>/<date_e>', views.get_data_result, name='get_data_result'),
    path('irrigation/result/sensor_series/<farm_id>/<sector_id>/<date>', views.get_series_result, name='get_series_result'),

    path('show', views.show_simple_upload, name='import_data'),
    path('import/sensors', views.import_data_sensor, name='import_data_to_sensors'),
    path('import/crop_soil', views.import_crop_soil_properties, name='import_crop_soil_properties'),
    # path('save', views.simple_upload, name='save_person'),

    path('get/html/farm', views.get_html_add_farm, name='get_html_farm'),
    path('save/farm', views.save_farm, name='save_farm'),

    path('get/html/sector/<farm_id>', views.get_html_add_sector, name='get_html_sector'),
    path('save/sector', views.save_sector, name='save_sector'),
    path('save/features', views.save_features, name='save_features'),

    path('get/farms/', views.get_farms, name='get_farms'),
    path('get/features/<sector_id>', views.get_features, name='get_features'),

    path('get/sector/<farm_id>', views.get_sectors, name='get_sectors'),

    path('get/crop/<crop_group>', views.get_crops_from_group, name='get_crops_from_group'),
    path('get/crop_option/<crop_group>/<crop>', views.get_crops_options_from_group, name='get_crops_options_from_group'),
]
