from django.contrib import admin


# Register your models here.
from DBAPI.models import Farm, IrrigationSector
from DBAPI.models.crop_type import CropType
from DBAPI.models.soil_type import SoilType

admin.site.register(Farm)
admin.site.register(IrrigationSector)
admin.site.register(SoilType)
admin.site.register(CropType)
