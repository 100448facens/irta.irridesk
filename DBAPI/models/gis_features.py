from djongo import models


# from DBAPI.models import IrrigationSector


class GisFeature(models.Model):
    id = models.AutoField(primary_key=True)
    x = models.FloatField()
    y = models.FloatField()

    def __str__(self):
        return 'x = {} y = {}'.format(self.x, self.y)

    class Meta:
        ordering = ('x', 'y')

