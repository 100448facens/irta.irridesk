from djongo import models

from DBAPI.models import IrrigationSector


class Sensor(models.Model):
    id = models.AutoField(primary_key=True)
    irrigation_sector = models.ForeignKey(to=IrrigationSector, on_delete=models.CASCADE)
    label = models.CharField(max_length=100)

    # init_at = models.DateTimeField(auto_now_add=True, blank=True)
    # end_at = models.DateTimeField(auto_now_add=True, blank=True)
    # data = models.TextField(max_length=100000, blank=True)

    @classmethod
    def create(cls, label, irrigation_sector):
        """ The param irrigation_sector is of type IrrigationSectorModel, so convert to object IrrigationSector """
        sector = IrrigationSector.objects.get(id=irrigation_sector.id)

        s = cls(label=label, irrigation_sector=sector)

        return s

    def __str__(self):
        return 'id={} label={}'.format(self.id, self.label)
