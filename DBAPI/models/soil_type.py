from djongo import models


class SoilType(models.Model):
    id = models.AutoField(primary_key=True)

    soil_type = models.CharField(max_length=100)
    swc_fc = models.FloatField()
    swc_wp = models.FloatField()

    @classmethod
    def create(cls, soil_type, swc_fc, swc_wp):
        soil = cls(soil_type=soil_type, swc_fc=swc_fc, swc_wp=swc_wp)
        soil.save()
        return soil

    # additional_properties = models.ListField()
    class Meta:
        ordering = ('soil_type',)
