from djongo import models
from DBAPI.models.sensor import Sensor


class Plot(models.Model):
    name = models.CharField(max_length=100)
    sensors = models.ArrayReferenceField(to=Sensor, on_delete=models.CASCADE)

