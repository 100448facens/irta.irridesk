from djongo import models

from DBAPI.models.farm import Farm
from DBAPI.models.crop_properties import CropProperties

from DBAPI.models.irrigation_system_properties import IrrigationSystemProperties
from DBAPI.models.soil_properties import SoilProperties


class IrrigationSector(models.Model):
    label = models.CharField(max_length=255)
    farm = models.ForeignKey(to=Farm, on_delete=models.CASCADE)
    crop = models.ForeignKey(to=CropProperties, on_delete=models.CASCADE, blank=True)
    soil = models.ForeignKey(to=SoilProperties, on_delete=models.CASCADE, blank=True)
    irrigation_setup = models.ForeignKey(to=IrrigationSystemProperties, on_delete=models.CASCADE, blank=True)
    gis_features = models.TextField(max_length=10000, blank=True)

    @classmethod
    def create(cls, label, farm):
        """ The param farm is of type FarmModel, so convert to object Farm """
        f = Farm.objects.get(id=farm.id)

        irr = cls(label=label, farm=f)

        return irr

    @classmethod
    def set_crop(cls, crop_properties):
        # cls.crop.add(crop_properties)
        cls.crop.set(crop_properties)

    @classmethod
    def set_soil(cls, soil_properties):
        # cls.crop.add(crop_properties)
        cls.soil.set(soil_properties)

    @classmethod
    def set_irrigation_setup(cls, irrigation_properties):
        # cls.crop.add(crop_properties)
        cls.irrigation_setup.set(irrigation_properties)

    def __str__(self):
        return 'id={} label={} gis_features={}'.format(self.id, self.label, self.gis_features)
