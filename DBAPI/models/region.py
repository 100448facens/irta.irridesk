from djongo import models
from DBAPI.models.plot import Plot


class Region(models.Model):
    name = models.CharField(max_length=100)
    plots = models.ArrayReferenceField(to=Plot, on_delete=models.CASCADE)

    @classmethod
    def create(cls, name):
        region = cls(name=name)
        return region
