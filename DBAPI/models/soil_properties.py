from djongo import models


class SoilProperties(models.Model):
    id = models.AutoField(primary_key=True)

    soil_type = models.CharField(max_length=100)
    soil_depth = models.FloatField(blank=True)
    stone_content = models.FloatField(blank=True)
    swc_fc = models.FloatField(blank=True, default=0.0)
    swc_wp = models.FloatField(blank=True, default=0.0)
    # soil_type = models.ForeignKey(to=SoilType, on_delete=models.CASCADE)

    # TODO: ask to Jaume about this dictionary
    # additional_properties = models.ListField()
