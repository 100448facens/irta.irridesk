from djongo import models

from DBAPI.models.crop_type import CropType


class CropProperties(models.Model):
    id = models.AutoField(primary_key=True)

    crop_type = models.CharField(max_length=255, default="")
    crop = models.CharField(max_length=255, default="")
    crop_options = models.CharField(max_length=255, default="")
    # crop = models.ForeignKey(to=CropType, on_delete=models.CASCADE)

    distance_trees = models.FloatField()
    distance_lines = models.FloatField()
    plantation_year = models.IntegerField()

    tree_width = models.FloatField()
    tree_depth = models.FloatField()
    tree_height = models.FloatField()

    start_vegetative_period = models.DateTimeField()
    flowering_date = models.DateTimeField()
    start_harvest = models.DateTimeField()
    end_harvest = models.DateTimeField()

    # TODO: ask to Jaume about this dictionary
    # additional_properties = models.ListField()

    @classmethod
    def create(cls, crop_type, crop, crop_options, distance_trees, distance_lines, plantation_year, tree_width,
               tree_depth, tree_height, start_vegetative_period, flowering_date, start_harvest, end_harvest):

        crop = cls(crop_type=crop_type, crop=crop, crop_options=crop_options, distance_trees=distance_trees,
                   distance_lines=distance_lines, plantation_year=plantation_year, tree_width=tree_width,
                   tree_height=tree_height, tree_depth=tree_depth, start_vegetative_period=start_vegetative_period,
                   start_harvest=start_harvest, flowering_date=flowering_date, end_harvest=end_harvest)

        crop.save()
        return crop
