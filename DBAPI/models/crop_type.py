from djongo import models


class CropType(models.Model):
    id = models.AutoField(primary_key=True)

    crop_type = models.CharField(max_length=255, default="")
    crop = models.CharField(max_length=255, default="")
    crop_options = models.CharField(max_length=255, default="", blank=True)

    @classmethod
    def create(cls, crop_type, crop, crop_options):
        crop = cls(crop_type=crop_type, crop=crop, crop_options=crop_options)
        crop.save()
        return crop

    # additional_properties = models.ListField()
    class Meta:
        ordering = ('crop_type', )
