from djongo import models


class IrrigationSystemProperties(models.Model):
    id = models.AutoField(primary_key=True)

    drip_lines = models.FloatField()
    dist_drippers = models.FloatField()
    drip_flow = models.FloatField()
    efficiency = models.FloatField()
    wetted_diameter = models.FloatField()
    max_allocation = models.FloatField()

    # TODO: ask to Jaume about this dictionary
    # additional_properties = models.ListField()
