from djongo import models
from django.contrib.auth.models import Group


class Farm(models.Model):
    id = models.AutoField(primary_key=True)
    label = models.CharField(max_length=255)
    group = models.ForeignKey(Group, on_delete=models.CASCADE)

    @classmethod
    def create(cls, label, group):
        farm = cls(label=label, group=group)
        farm.save()
        return farm
