from djongo import models

from DBAPI.models import IrrigationSector


class SensorData(models.Model):
    id = models.AutoField(primary_key=True)
    irrigation_sector = models.ForeignKey(to=IrrigationSector, on_delete=models.CASCADE)
    acumm_irr = models.FloatField(blank=True, default=0.0)
    irr_day = models.FloatField(blank=True, default=0.0)
    eto = models.FloatField(blank=True, default=0.0)
    irr_ok = models.FloatField(blank=True, default=0.0)
    plan_ok = models.FloatField(blank=True, default=0.0)
    sensor_ok = models.FloatField(blank=True, default=0.0)
    all_ok = models.FloatField(blank=True, default=0.0)
    date = models.DateTimeField(blank=True)

    @classmethod
    def create(cls, irrigation_sector_id, acumm_irr, irr_day, eto, irr_ok, plan_ok, sensor_ok, all_ok, date):

        sector = IrrigationSector.objects.get(id=irrigation_sector_id)

        s = cls(irrigation_sector=sector, acumm_irr=acumm_irr, irr_day=irr_day, eto=eto, irr_ok=irr_ok, plan_ok=plan_ok,
                sensor_ok=sensor_ok, all_ok=all_ok, date=date)

        return s

    def __str__(self):
        return "id={} acumm_irr={} irr_day={} eto={} irr_ok={} plan_ok={} sensor_ok={} all_ok={} data={}". \
            format(self.id, self.acumm_irr, self.irr_day, self.eto, self.irr_ok, self.plan_ok, self.sensor_ok,
                   self.all_ok, self.date)
