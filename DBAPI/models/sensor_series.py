from djongo import models

from DBAPI.models import Sensor


class SensorSeries(models.Model):
    id = models.AutoField(primary_key=True)
    sensor = models.ForeignKey(to=Sensor, on_delete=models.CASCADE)
    init_at = models.DateTimeField(blank=True)
    end_at = models.DateTimeField(blank=True)
    data = models.TextField(max_length=100000, blank=True)

    @classmethod
    def create(cls, sensor_id, init_at, end_at, data):

        sensor = Sensor.objects.get(id=sensor_id)

        s = cls(sensor=sensor, init_at=init_at, end_at=end_at, data=data)

        return s

    def __str__(self):
        return "id={} init_at={} end_at={} data={}".format(self.id, self.init_at, self.end_at, self.data)
