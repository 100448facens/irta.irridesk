from rest_framework import serializers

from DBAPI.models import Farm, IrrigationSector, Sensor
from DBAPI.models.crop_type import CropType
from DBAPI.models.sensor_data import SensorData
from DBAPI.models.sensor_series import SensorSeries
from DBAPI.models.soil_type import SoilType


class FarmSerializer(serializers.ModelSerializer):
    class Meta:
        fields = '__all__'
        model = Farm


class IrrigationSectorSerializer(serializers.ModelSerializer):
    class Meta:
        fields = '__all__'
        model = IrrigationSector


class CropTypeSerializer(serializers.ModelSerializer):
    class Meta:
        fields = '__all__'
        model = CropType


class SoilTypeSerializer(serializers.ModelSerializer):
    class Meta:
        fields = '__all__'
        model = SoilType


class SensorSerializer(serializers.ModelSerializer):
    class Meta:
        fields = '__all__'
        model = Sensor


class SensorDataSerializer(serializers.ModelSerializer):
    class Meta:
        fields = '__all__'
        model = SensorData


class SensorSeriesSerializer(serializers.ModelSerializer):
    class Meta:
        fields = '__all__'
        model = SensorSeries
